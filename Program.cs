﻿using System;
using System.Net;

namespace OTUS_RegEx
{
    class Program
    {
        static void Main(string[] args)
        {
            string httpPath = Console.ReadLine();
            HtmlDownloader hd = new HtmlDownloader();
            Console.WriteLine("Hello World!");
            Uri url = new Uri(httpPath);
            hd.GetHtml(url);
            //System.Console.WriteLine($"{hd.ImagePathList}");

            ImageCreator imageCreator = new ImageCreator(hd.ImagePathList);
            imageCreator.downloadAndCreateImages();
        }
        
    }
}
