﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace OTUS_RegEx
{
    class ImageCreator
    {
        private List<string> _downloadFilesPath = new List<string>();
        private string _imageDir = default; // путь к папке где будем хранить изображения
        public ImageCreator(List<string> pathsToDownloadFiles)
        {
            _downloadFilesPath = pathsToDownloadFiles;
        }
        public void downloadAndCreateImages()
        {
            using (WebClient wb = new WebClient())
            {
                var desctopFolder = Environment.CurrentDirectory;// TODO получить текущую папку чтобы от неё добраться до image
                string appDirectory = Environment.CurrentDirectory; // Получае текущую директорию где выполняется программа
                string[] foldersList = Directory.GetDirectories(appDirectory); // получаем список всех папок 
                foreach (string curFolder in foldersList)
                {
                    if (curFolder.EndsWith("Images")) // если папка с именем Images существует то сохраняем картинки в неё
                    {
                        _imageDir = curFolder; // получаем имя папки с которой будем работать
                        break;
                    }
                }
                if (_imageDir == default)// Если папка не была найдена в директории
                {
                    _imageDir = Path.Combine(appDirectory, @"Images");// генерируем имя папки
                    Directory.CreateDirectory(_imageDir); // создаём новую папку с именем Images
                }
                for (int i = 0; i < _downloadFilesPath.Count; i++)
                {
                    wb.DownloadFile(_downloadFilesPath[i], Path.Combine(_imageDir, $"DownloadFile_{i}.jpg"));
                }
            }
        }

    }
}
