﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;

namespace OTUS_RegEx
{
    class HtmlDownloader
    {
        private List<string> _imagePathList = new List<string>();
        public List<string> ImagePathList 
        {
            get
            {
                return _imagePathList;   
            } 
            set
            {
                _imagePathList = value;
            }
         }
        public void GetHtml(Uri url)
        {
            using (WebClient webClient = new WebClient())
            {
                Regex regex = new Regex(@"<img.+src ?= ?[""']{0,2}(?<url>.+?)[""']", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                var sourceCode = webClient.DownloadString(url);
                MatchCollection collection = regex.Matches(sourceCode);
                foreach (Match curElement in collection)
                {
                    _imagePathList.Add(curElement.Groups["url"].Value);
                }
            }
        }

    }
}
